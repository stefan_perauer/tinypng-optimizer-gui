﻿# TinyPngOptimizer

## Purpose

This program is a simple GUI for the TinyPng API https://tinypng.com/developers.
It will recurse through all directories and optimze all JPGs and PNGs in it.
You need to get an API key for TinyPng to use it.