﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace TinyPngImageOptimizer
{
    public partial class MainWindow
    {
        private const string StateFileName = "state.txt";

        private Task _task;
        private string _startDirectory = "";// readable in background tasks
        private List<string> _converted = new List<string>();

        private string StartDirectory { get { return DirectoryTextBox.Text ?? ""; } }

        private string ConvertedFilesFileName { get { return Path.Combine(StartDirectory, ".optimized-images"); } }

        private string ApiKey { get { return ApiKeyTextBox.Text ?? ""; } }


        public MainWindow()
        {
            InitializeComponent();

            RestoreTextBoxes();

            RestoreConvertedList();

            Application.Current.Exit += OnAppExit;
        }

        private void OnAppExit(object sender, ExitEventArgs e)
        {
            WriteConvertedList();
        }

        private void StartOptimize()
        {
            if (!Directory.Exists(StartDirectory))
            {
                MessageBox.Show(string.Format("Directory {0} not found", StartDirectory));
                return;
            }

            if (string.IsNullOrWhiteSpace(ApiKey))
            {
                MessageBox.Show("Please enter your TinyPng/TinyJpg API key");
                return;
            }

            RestoreConvertedList();

            StartOptimizeButton.IsEnabled = false;
            ProgressBar.IsIndeterminate = true;
            ResultsList.Items.Clear();

            _startDirectory = StartDirectory;
            var key = ApiKey;

            _task = Task.Factory.StartNew(() =>
            {
                try
                {
                    ImageOptimizer.Optimize(new DirectoryInfo(_startDirectory), key, ShouldConvert, AddResult, ErrorCallback);
                }
                catch (Exception ex)
                {
                    Invoke(() => MessageBox.Show("Exception " + ex.Message));
                }
                finally
                {
                    Invoke(() =>
                    {
                        WriteConvertedList();
                        StartOptimizeButton.IsEnabled = true;
                        ProgressBar.IsIndeterminate = false;
                    });
                }
            });
        }

        private bool ShouldConvert(string file)
        {
            var isJpeg = (file.ToUpperInvariant().EndsWith("JPEG") || file.ToUpperInvariant().EndsWith("JPG"));
            var isPng = (file.ToUpperInvariant().EndsWith("PNG"));

            if (_converted.Contains(GetFileNameRelativeToStartDir(file)))
            {
                AddToResultList("Skipping", GetFileNameRelativeToStartDir(file) + " as it was already optimized");
                return false;
            }

            return (isJpeg || isPng);
        }

        private void ErrorCallback(Exception obj)
        {
            AddToResultList("Exception", obj.Message);
        }

        private void AddToResultList(string left, string right)
        {
            Invoke(() => ResultsList.Items.Add(
                new TextBox()
                {
                    Background = new SolidColorBrush(Colors.Transparent),
                    BorderThickness = new Thickness(0),
                    IsReadOnly = true,
                    Text = string.Format("{0} {1}", left.PadLeft(9, ' '), right),
                    FontFamily = new FontFamily("Courier New"),
                }));

        }

        private void AddResult(string fileName, double compression)
        {
            AddToResultList(compression.ToString("P"), GetFileNameRelativeToStartDir(fileName));
            AddToConverted(fileName);
        }


        private void RestoreConvertedList()
        {
            try
            {
                _converted = File.ReadAllLines(ConvertedFilesFileName).ToList();
            }
            catch (Exception)
            { }
        }
        private void WriteConvertedList()
        {
            try
            {
                File.WriteAllLines(ConvertedFilesFileName, _converted);
            }
            catch (Exception)
            {
            }
        }

        private void AddToConverted(string fileName)
        {
            _converted.Add(GetFileNameRelativeToStartDir(fileName));
        }

        private string GetFileNameRelativeToStartDir(string fileName)
        {
            return fileName.Replace(_startDirectory, "").TrimStart(new[] { '\\' });
        }

        private void StartOptimizeClick(object sender, RoutedEventArgs e)
        {
            if (_task != null && !_task.IsCompleted)
                return;

            StartOptimize();
        }

        private void SelectDirectoryClick(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog { IsFolderPicker = true };
            var result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                DirectoryTextBox.Text = dialog.FileNames.FirstOrDefault();
                SaveTextBoxes();
            }
        }

        private void SaveTextBoxes()
        {
            try
            {
                File.WriteAllText(StateFileName, string.Join("\n", new[] { StartDirectory, ApiKey }));
            }
            catch (Exception)
            { }
        }

        private void RestoreTextBoxes()
        {
            try
            {
                var text = File.ReadAllText(StateFileName).Split(new[] { '\n' });
                DirectoryTextBox.Text = text.First();
                ApiKeyTextBox.Text = text.Skip(1).First();
            }
            catch (Exception)
            { }
        }


        public void Invoke(Action meth)
        {
            Dispatcher.BeginInvoke(meth, null);
        }

        private void ApiKeyTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            SaveTextBoxes();
        }
    }
}
