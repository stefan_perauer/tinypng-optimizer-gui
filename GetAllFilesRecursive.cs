﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TinyPngImageOptimizer
{
    public class GetAllFilesRecursive
    {
        public static IEnumerable<string> GetFiles(string path, Action<Exception> errorCallback = null)
        {
            var queue = new Queue<string>();
            queue.Enqueue(path);

            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (Exception ex)
                {
                    if (errorCallback != null)
                        errorCallback(ex);
                }

                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path);
                }
                catch (Exception ex)
                {
                    if (errorCallback != null)
                        errorCallback(ex);
                }

                if (files != null)
                {
                    foreach (string file in files)
                    {
                        yield return file;
                    }
                }
            }
        }
    }
}
