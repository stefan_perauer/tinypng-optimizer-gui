﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace TinyPngImageOptimizer
{
    public class ImageOptimizer
    {
        public static void Optimize(DirectoryInfo dir, string apiKey, Func<string, bool> shouldConvertCallback, Action<string, double> fileResultCallback, Action<Exception> errorCallback)
        {
            foreach (var file in GetAllFilesRecursive.GetFiles(dir.FullName, errorCallback))
            {
                try
                {
                    var size = new FileInfo(file).Length;

                    if (!shouldConvertCallback(file))
                        continue;

                    Optimize(file, file, apiKey);

                    var newSize = new FileInfo(file).Length;

                    fileResultCallback(file, 1.0d - (double)newSize / size);
                }
                catch (Exception ex)
                {
                    errorCallback(ex);
                }
            }
        }


        static void Optimize(string input, string output, string apiKey)
        {
            string url = "https://api.tinypng.com/shrink";

            var client = new WebClient();

            string auth = Convert.ToBase64String(Encoding.UTF8.GetBytes("api:" + apiKey));
            client.Headers.Add(HttpRequestHeader.Authorization, "Basic " + auth);

            client.UploadData(url, File.ReadAllBytes(input));
            /* Compression was successful, retrieve output from Location header. */
            client.DownloadFile(client.ResponseHeaders["Location"], output);
        }
    }
}
